# convert tab-delimited product data to JSON format
use JSON;

open PRODUCTS_IN, '<data/products.csv';
while (<PRODUCTS_IN>) {
    next if $. == 1;
    @item{qw(description lining price image)} = map s/^\s+|\s+$//gr, split /\t/;
    if ($item{image} !~ /-225/) {
        $dot_pos = index $item{image}, '.';
        $basename = substr $item{image}, 0, $dot_pos;
        $suffix = substr $item{image}, $dot_pos;
        $item{image} = "$basename-225$suffix";
    }
    
    $item{price} += 0;
    push @items, { %item };
}
close PRODUCTS_IN;

open PRODUCTS_OUT, '>data/products.json';
print PRODUCTS_OUT JSON->new->canonical->pretty->encode(\@items);
close PRODUCTS_OUT;
