use File::Basename;

for $image(<images/*.jpg>) {
    next if $image =~ /-225/;
    ($basename, $path, $suffix) = fileparse $image, '.jpg';
    $newpath = "$path${basename}-225$suffix";
    print "converting $image -> $newpath\n";
    system 'convert', $image, '-resize', 'x225', $newpath and warn "Couldn't convert $image: $!";
}
