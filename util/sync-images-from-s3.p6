use v6.c;

my constant $MEDIA-FILES-BUCKET = 's3://chacewells-comfy-horn-strap-media-files';
my @media-files = run(«aws s3 ls "$MEDIA-FILES-BUCKET/images/"», :out).out.lines.map(*.split(/\s+/)[*-1]);
my @s3-paths = @media-files.map({"$MEDIA-FILES-BUCKET/images/$_"});

for @s3-paths -> $s3-path { run «aws s3 cp $s3-path images/» }
'images/'.IO.dir».chmod(0o755);
