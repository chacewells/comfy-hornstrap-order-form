const SHIPPING_PRICE_PER_STRAP = 4.5;

let thumbnailElement,
    itemPriceElement,
    quantityControl,
    styleControl,
    subtotalControl,
    reminderSubtotalControl,
    shippingControl,
    reminderShippingControl,
    totalPriceControl,
    reminderTotalPriceControl,
    billingAddressBlock,
    orderForm;

let products;

let isBillingAddress = false;

let addressInputNames = ['address1', 'address2', 'city', 'state', 'zip'];

window.addEventListener('load', function () {
    thumbnailElement = document.getElementById('thumbnail');
    itemPriceElement = document.getElementById('item-price');
    quantityControl = document.getElementById('quantity');
    styleControl = document.getElementById('style');
    subtotalControl = document.getElementById('subtotal');
    shippingControl = document.getElementById('shipping');
    totalPriceControl = document.getElementById('total-price');
    reminderSubtotalControl = document.getElementById('reminder-subtotal');
    reminderShippingControl = document.getElementById('reminder-shipping');
    reminderTotalPriceControl = document.getElementById('reminder-total-price');

    billingAddressBlock = document.getElementById('billing-address');
    orderForm = document.forms['order-form'];

    // initialization steps
    populateStyleControl();
    loadForm();
    updateItemPrice();
    updateThumbnail();
});

// lazy load product data
function getProductData() {
    return new Promise(function (resolve, reject) {
        if (products) {
            resolve(products);
        } else {
            // rest call
            // let productsLink = document.getElementById('product-data');
            // products = JSON.parse(productsLink.import.body.innerText);
            let productsRequest = new XMLHttpRequest();
            productsRequest.addEventListener('load', function () {
                // TODO handle error scenarios
                let productsData = this.responseText;
                products = JSON.parse(productsData);
                resolve(products);
            });

            productsRequest.open('GET', 'data/products.json');
            productsRequest.send();
        }
    });
}

// add options to <select> element
function populateStyleControl() {
    getProductData().then(function (products) {
        products.forEach( (product, i) => {
            let option = document.createElement('option');
            let optionText = `${product.description} ($${product.price.toFixed(2)})`;
            option.innerText = optionText;
            option.value = i;
            styleControl.appendChild(option);
        });
    })
}

function onColorSelected() {
    updateThumbnail();
}

function updateThumbnail() {
    getProductData().then(function (products) {
        let product = products[styleControl.value];
        let filename = product.image || '';
        thumbnailElement.src = `images/${filename}`;
        thumbnailElement.alt = product.description;
        updateItemPrice();
    });
}

function onQuantityChanged() {
    updatePrices();
}

function updatePrices() {
    let quantity = quantityControl.value;
    getProductData().then(function (products) {
        let product = products[styleControl.value];

        if (quantity < 3)
            shipping = SHIPPING_PRICE_PER_STRAP * quantity;
        else
            shipping = 0;

        subtotal = product.price * quantity;
        totalPrice = subtotal + shipping;

        subtotalControl.value = subtotal.toFixed(2);
        shippingControl.value = shipping.toFixed(2);
        totalPriceControl.value = totalPrice.toFixed(2);
        reminderSubtotalControl.value = subtotal.toFixed(2);
        reminderShippingControl.value = shipping.toFixed(2);
        reminderTotalPriceControl.value = totalPrice.toFixed(2);
    });
}

function updateItemPrice() {
    getProductData().then(function (products) {
        let product = products[styleControl.value];
        itemPriceElement.innerText = product.price.toFixed(2);
        updatePrices();
    });
}

function onToggleIsBillingAddress(e) {
    isBillingAddress = !isBillingAddress;
    billingAddressBlock.disabled = isBillingAddress;
    if (isBillingAddress) copyAddressFromShippingToBilling();
}

function copyAddressFromShippingToBilling() {
    addressInputNames.forEach(name => {
        orderForm.elements.namedItem(`billing-${name}`).value = orderForm.elements.namedItem(`shipping-${name}`).value;
    });
}

function onSaveForLater() {
    for (let i = 0; i < orderForm.elements.length; ++i) {
        let e = orderForm.elements.item(i);
        localStorage.setItem(e.name, e.value);
    }

    let quantity = orderForm.elements.namedItem('quantity').value || 0;
    getProductData().then(function (products) {
        let product = products[styleControl.value];

        window.alert(`Your order for ${quantity} '${product.description}' Comfy Horn Strap${quantity != 1 ? 's' : ''} has been saved!`);
    });
}

function loadForm() {
    for (let i = 0; i < orderForm.elements.length; ++i) {
        let e = orderForm.elements.item(i);
        let localStorageValue = localStorage.getItem(e.name);
        if (localStorageValue) {
            console.log(`setting ${e.name} = ${localStorageValue}`);
            e.value = localStorageValue;
        }
    }
}

function onClearForm() {
    for (let i = 0; i < orderForm.elements.length; ++i) {
        let e = orderForm.elements.item(i);
        e.value = '';
        localStorage.removeItem(e.name);
    }
}

